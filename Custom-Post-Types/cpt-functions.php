<?php
//Copy this code into the functions.php-File of your Wordpress-Theme or use the include-Function of php for that
 
// Register Custom Post Type
function gen_cpt_redner() {

	$labels = array(
		'name'                => 'Redner',
		'singular_name'       => 'Redner',
		'menu_name'           => 'Redner',
		'parent_item_colon'   => 'Redner',
		'all_items'           => 'Alle Redner',
		'view_item'           => 'Redner anzeigen',
		'add_new_item'        => 'Neuen Redner anlegen',
		'add_new'             => 'Neuen Redner anlegen',
		'edit_item'           => 'Redner bearbeiten',
		'update_item'         => 'Redner aktualisieren',
		'search_items'        => 'Redner suchen',
		'not_found'           => 'Kein Redner gefunden',
		'not_found_in_trash'  => 'Kein Redner gefunden im Papierkorb',
	);
	$rewrite = array(
		'slug'                => 'redner',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'label'               => 'Redner',
		'description'         => 'Ermöglicht das Anlegen und Ausgeben von Rednern',
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-admin-users',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
	);
	register_post_type( 'Redner', $args );

}
// Hook into the 'init' action
add_action( 'init', 'gen_cpt_redner', 0 );

add_action( 'admin_init', "cpt_redner_meta_boxen" );
add_action( 'save_post', "cpt_redner_daten_speichern" );

function cpt_redner_meta_boxen(){
	add_meta_box("kontakt-meta", "Kontaktdaten", "cpt_redner_feld_kontakt", "redner", "side", "low");
}
function cpt_redner_feld_kontakt(){
	global $post;
	$custom = get_post_custom($post->ID);
	$kontaktfeld = $custom["kontaktfeld"][0];
	echo '<textarea name="kontaktfeld">'. $kontaktfeld . '</textarea>';
}
function cpt_redner_daten_speichern(){
	global $post;
	update_post_meta($post->ID, "redner", $_POST["kontaktfeld"]);
}
?>
